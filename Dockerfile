FROM node:16-slim
LABEL maintainer="ISP RAS"

WORKDIR /app/
RUN apt update &&\
    apt install git -y
RUN echo v=0.1
RUN git clone https://gitlab.com/AlexanderMishutkin/a4c-clouni-plugin.git
WORKDIR /app/a4c-clouni-plugin
RUN npm i
RUN apt remove git -y &&\
    apt autoremove -y &&\
    apt clean
ARG API=http://nevbros.ru
RUN echo API=$API > .env
EXPOSE 3000
CMD npm run dev
