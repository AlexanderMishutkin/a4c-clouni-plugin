# Alien4Cloud plugin for [Clouni orchestrator](https://github.com/ispras/clouni)

### This server does 3 different tasks:
* It serves as proxy for Alien4Cloud (port 8088) and [SimpleClouniAPI](https://gitlab.com/AlexanderMishutkin/simple-clouni-api) (port 50051).
* It injects JS code, which we call the plugin, into Alien4Cloud dependency `/bower_components/requirejs/require.js`. This code overrides <b>Deploy</b> command.
* It provides its own Vue application (`/front/deploy?topology=...`), that helps to deploy topology with a clone.

### Use Case diagram

[Use Case diagram](https://gitlab.com/AlexanderMishutkin/a4c-clouni-plugin/-/raw/master/static/Use%20Case%20Diagram%20-%20EN.jpg)

## Develop Setup

You should create a `.env` file with the `API` (e.g. `API=http://nevbros.ru`) variable specified, which is actually the URL of the host, where a4c and Clouni run. 

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

## Product Setup

1. Firstly, you need to fetch or build [SimpleClouniAPI](https://gitlab.com/AlexanderMishutkin/simple-clouni-api) Docker container.
2. Then run it: `docker run -itd -p 50051:50051 clouni-api`
3. Run A4C: `docker run -itd -p 8088:8088 alien4cloud/alien4cloud:3.4.0`
4. Finally, run plugin: `docker run -itd -p 3000:3000 a4c-clouni-plugin`

All ports are fixed strictly.

## FAQ

* Why not create a common A4C plugin like one for Yorc orchestrator?

A4C is very poorly documented. The plugin is not allowed to change the REST API or the execution flow, only some methods in some services. On the other hand, to create an orchestrator plugin you need to rewrite the orchestrator service from the very beginning as it is done in the Yorc plugin. After all, you need to understand and write thousands of lines of code even to add the simplest functionality.

* Why not use the existing Clouni gRPC server?

It has a list of issues (at least it does not specify the Protobuf package) and in my own opinion, the idea of using gRPC for one call with a few arguments is a bit overkill. It's much simpler to maintain a simple Rest API than a gRPC server.

* How to run the server on port 80?

Since you can't change ports, you can only run two `a4c-clouni-plugin` containers with `-p 3000:3000` and `-p 3000:80` (middleware server is completely stateless). Another way is to start the development server and change every repetition of `:3000` in it. Yes, I know that both ways are bad.

### C4

[C4 diagram](https://gitlab.com/AlexanderMishutkin/a4c-clouni-plugin/-/raw/master/static/C4.png)

## TODO

* Add opportunity to set up TCP ports too.
* Add opportunity to switch off the plugin yet. (Yes, you can use the A4C on another port, but it is strange...)
* SimpleClouniAPI is a WIP project that should be finished or replaced by something else.
