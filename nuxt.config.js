require('dotenv').config()

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'a4c-clouni-plugin',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
  },

  publicRuntimeConfig: {
    API: process.env.API
  },
  privateRuntimeConfig: {
    API: process.env.API
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~assets/css/global.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/dotenv',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    ['@nuxtjs/proxy', {
      pathRewrite: { '^/bower_components/requirejs': '', '^/clouni': '' },
      changeOrigin: false,
      proxyHeadersIgnore: [], proxyHeaders: false
    }]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  proxy: [
    [
      'localhost:3000/bower_components/requirejs/**', {
        target: 'http://localhost:3000',
        changeOrigin: false,
        proxyHeadersIgnore: [], proxyHeaders: false
      }
    ],
    [
      'localhost:3000/clouni/**', {
        target: process.env.API + ':50051',
        changeOrigin: false,
        proxyHeadersIgnore: [], proxyHeaders: false
      }
    ],
    [
      ['/**', '!/front/**', '!/back/**', '!/_*/**'], {
        target: process.env.API + ':8088/',
        changeOrigin: false,
        proxyHeadersIgnore: [], proxyHeaders: false
      }
    ],
  ],

  serverMiddleware: [
  ],

  server: {
    host: '0.0.0.0',
  }
}
